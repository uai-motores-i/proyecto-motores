using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{

    private GameObject Jugador;
    public int rapidez;

    void Start()
    {

        Jugador = GameObject.Find("Jugador");
    }

    private void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
}
