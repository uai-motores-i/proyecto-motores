using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public float rapidezSprint = 20.0f;
    private int rango = 8;
    private int cont;
    Vector3 scaleParado;
    Vector3 scaleCrouch;
    bool agachado;
    public AudioSource Sonidopasos;
    private bool HActivo;
    private bool Vactivo;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        scaleParado = transform.localScale;
        scaleCrouch = scaleParado;
        scaleCrouch.y = scaleParado.y * .10f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            other.gameObject.SetActive(false);
        }
    }


    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        agachado = Input.GetKey(KeyCode.LeftControl);
        transform.localScale = agachado ? scaleCrouch : scaleParado;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            rapidezDesplazamiento = rapidezSprint;

        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            rapidezDesplazamiento = 10.0f;
        }


        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetButtonDown("Horizontal"))
        {
            HActivo = true;

            Sonidopasos.Play();
        }

        if (Input.GetButtonDown("Vertical"))
        {
            Vactivo = true;
            Sonidopasos.Play();
        }

        if (Input.GetButtonUp("Horizontal"))
        {
            HActivo = false;
            if (Vactivo == false)
            {
                Sonidopasos.Pause();
            }
            
        }

        if (Input.GetButtonUp("Vertical"))
        {
            Vactivo = false;
            if (HActivo == false)
            {
                Sonidopasos.Pause();
            }
            
        }
;


        RaycastHit hit;
        if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, rango))
        {
            if (hit.collider.GetComponent<Enemigo>() == true)
            {
                hit.collider.GetComponent<Enemigo>().enabled = false;
            }

            if (hit.collider.GetComponent<Enemigo>() == true)
            {
                hit.collider.GetComponent<Enemigo>().enabled = true;
            }

          
           
        }
    }



}
